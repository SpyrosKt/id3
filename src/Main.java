
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		final double TRAIN_PERCENTAGE = 0.05;
		final double VALIDATION_PERCENTAGE = 0.05;
		
		Dataset wholeDataset, trainData, validationData, testData;
		List<Double> thresholds = new ArrayList<>(Arrays.asList(0.005, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.2, 1.4, 1.6));
		DecisionTree validationTree, tree;
		
		
		Scanner sc = new Scanner(System.in);
		String path;
		while(true) {
			System.out.println("Absolute dataset path:");
			path = sc.nextLine();
			
			try {
				wholeDataset = new Dataset(path);
				testData = new Dataset(path);
				trainData = testData.extract((int) (testData.entries() * TRAIN_PERCENTAGE));
				
				tree = new DecisionTree(trainData, 0);
				
				validationData = testData.extract((int) (testData.entries() * VALIDATION_PERCENTAGE));
				validationTree = DecisionTree.defaultInstance(trainData, validationData, thresholds);
				
				System.out.println("Keeping train and validation data in test:");
				System.out.println("With validation:\t" + validationTree.test(testData));
				System.out.println("Without validation:\t" + tree.test(testData));
				System.out.println();
				
				System.out.println("Removing train and validation data from test:");
				System.out.println("With validation:\t" + validationTree.test(wholeDataset));
				System.out.println("Without validation:\t" + tree.test(wholeDataset));
				
				break;
			} catch(IOException e) {
				System.out.println("Error reading dataset.");
			}
		}
		
	}
	
}
