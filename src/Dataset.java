
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Dataset {
	
	private List<String> attributeNames = new ArrayList<>();
	// each List<String> represent the entries. e.g. contents.copy(5).copy(2) returns the the second attribute of the fifth entry
	private List<List<String>> contents = new ArrayList<>();
	private List<Set<String>> possibleValues = new ArrayList<>();
	private int classIndex;
	private double classEntropy = -1.0;
	
	private static final double LOG_OF_TWO = Math.log(2);
	
	/**
	 * Creates a dataset from an excel-like structured text file
	 * @param filePath
	 * @throws IOException	In case the file could not be found/read
	 */
	Dataset(String filePath) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		
		String currentEntry, currentElement;
		StringTokenizer tokenizedEntry;
		
		// the file's first line contains the attributes' names - supposedly
		if((currentEntry = br.readLine()) != null) {
			tokenizedEntry = new StringTokenizer(currentEntry, ",");
			this.classIndex = attributeNames.size() - 1;
			int tokenIndex = 0;
			while(tokenizedEntry.hasMoreTokens()) {
				currentElement = tokenizedEntry.nextToken();
				attributeNames.add(currentElement);
				
				// a set of possible values gets created for each attribute
				possibleValues.add(new TreeSet<>());
				
				currentElement = currentElement.replaceAll("[\"\']", "");
				if(currentElement.equalsIgnoreCase("class") || currentElement.equalsIgnoreCase("category"))
					this.classIndex = tokenIndex;
				tokenIndex++;
			}
		}
		
		while((currentEntry = br.readLine()) != null) {
			tokenizedEntry = new StringTokenizer(currentEntry, ",");
			contents.add(new ArrayList<>());
			int tokenIndex = 0;
			while(tokenizedEntry.hasMoreTokens()) {
				currentElement = tokenizedEntry.nextToken();
				contents.get(contents.size() - 1).add(currentElement);
				possibleValues.get(tokenIndex).add(currentElement);
				tokenIndex++;
			}
		}
	}
	
	// copy constructor
	Dataset(Dataset d) {
		this.attributeNames = new ArrayList<>(d.attributeNames);
		for(List entry : contents)
			this.contents.add(new ArrayList<>(entry));
		this.possibleValues = new ArrayList<>(d.possibleValues);
		this.classIndex = d.classIndex;
	}
	
	/**
	 * Returns a dataset consisting of entries form the prototype on which the given attribute (index) has the given value
	 * The new dataset has the initial dataset's (prototype) possibleValues, although not all may appear in it
	 * Furthermore, the attribute gets removed from each part's contents, attributeNames and possibleValues; the classIndex also gets adjusted
	 * @param prototype
	 * @param index	The index of the attribute based on which the new dataset will be created
	 * @param attributeValue
	 */
	Dataset(Dataset prototype, int index, String attributeValue) {
		this.attributeNames = new ArrayList<>(prototype.attributeNames);
		this.attributeNames.remove(index);
		
		this.possibleValues = new ArrayList<>(prototype.possibleValues);
		this.possibleValues.remove(index);
		
		// the case when index = initial.classIndex is also handled, although splitting by class shouldn't be a thing
		this.classIndex = (index <= prototype.classIndex) ? prototype.classIndex - 1 : prototype.classIndex;
		
		for(List<String> entry : prototype.contents) {
			if(entry.get(index).equals(attributeValue)) {
				this.contents.add(new ArrayList<>(entry));
				this.contents.get(this.contents.size() - 1).remove(index);
			}
		}
	}
	
	/**
	 * Creates a dataset from a content structure.
	 * The possibleValues List will consist of the values that appear in the given structure
	 * @param contents
	 */
	Dataset(List<String> attributeNames, List<List<String>> contents) {
		this.attributeNames = new ArrayList<>(attributeNames);
		
		this.classIndex = attributeNames.size() - 1;
		for(int i = 0; i < attributeNames.size(); i++)
			if(attributeNames.get(i).equalsIgnoreCase("class") || attributeNames.get(i).equalsIgnoreCase("category"))
				this.classIndex = i;
		
		for(int i = 0; i < contents.get(0).size(); i++)
			this.possibleValues.add(new TreeSet<>());
		
		for(List<String> entry : contents) {
			this.contents.add(new ArrayList<>());
			int tokenIndex = 0;
			for(String attribute : entry) {
				this.contents.get(this.contents.size() - 1).add(new String(attribute));
				this.possibleValues.get(tokenIndex).add(attribute);
				tokenIndex++;
			}
		}
	}
	
	/**
	 * Creates a dataset from a content and a possible values structure.
	 * The possibleValues List will be used as given, without any checks on the contents structure.
	 * @param contents
	 * @param possibleValues
	 */
	Dataset(List<String> attributeNames, List<List<String>> contents, List<Set<String>> possibleValues) {
		this.attributeNames = new ArrayList<>(attributeNames);
		this.possibleValues = new ArrayList<>(possibleValues);
		
		for(List<String> entry : contents) {
			this.contents.add(new ArrayList<>());
			for(String attribute : entry)
				this.contents.get(this.contents.size() - 1).add(new String(attribute));
		}
	}
	
	public String get(int entryIndex, int attributeIndex) {
		return contents.get(entryIndex).get(attributeIndex);
	}
	
	public String get(int entryIndex, String attributeName) {
		return get(entryIndex, getAttributeIndex(attributeName));
	}
	
	public List<String> getEntry(int index) {
		return new ArrayList<>(contents.get(index));
	}
	
	/**
	 * Maps each value of the entry at entryIndex to the appropriate String from attributeNames
	 * @param entryIndex The index of the entry to return
	 * @return The values of the entry at entryIndex, each mapped to its respective attributeName
	 */
	public Map<String, String> getMappedEntry(int entryIndex) {
		Map<String, String> mappedEntry = new TreeMap<>();
		for(int i = 0; i < attributes(); i++)
			mappedEntry.put(attributeNames.get(i), getEntry(entryIndex).get(i));
		return mappedEntry;
	}
	
	public List<List<String>> getContents() {
		List<List<String>> copy = new ArrayList<>();
		for(List<String> entry : contents)
			copy.add(new ArrayList<>(entry));
		return copy;
	}
	
	public List<String> getAttributeNames() {
		return new ArrayList<>(attributeNames);
	}
	
	public String getAttributeName(int attributeIndex) {
		return attributeNames.get(attributeIndex);
	}
	
	public String getClassName() {
		return attributeNames.get(classIndex);
	}
	
	public String classOf(int entryIndex) {
		return this.get(entryIndex, classIndex);
	}
	
	public int getClassIndex() {
		return classIndex;
	}
	
	public Set<String> getPossibleValues(String attributeName) {
		return possibleValues.get(attributeNames.indexOf(attributeName));
	}
	
	/**
	 * Splits the dataset based on the highest information gain attribute's value
	 * @return The sub-datasets, each mapped to the value (of the given attribute) that appears in it
	 */
	public Map<String, Dataset> split() {
		return split(indexOfHighestIGAtttribute());
	}
	
	/**
	 * Splits the dataset in parts based on the the given attribute's value
	 * This is done using the constructor designed for extracting parts of a dataset
	 * @param index	The index of the attribute to split the dataset
	 * @return The sub-datasets (parts), each mapped to the value (of the given attribute) that appears in it
	 */
	public Map<String, Dataset> split(int index) {
		Map<String, Dataset> parts = new TreeMap<>();
		Dataset currentPart;
		for(String attributeValue : possibleValues.get(index)) {
			currentPart = new Dataset(this, index, attributeValue);
			if(!currentPart.isEmpty())
				parts.put(attributeValue, currentPart);
		}
		return parts;
	}
	
	public Dataset copy(int fromIndex, int toIndex) {
		return new Dataset(this.attributeNames, contents.subList(fromIndex, toIndex));
	}
	
	// copies the given amount of entries to a new Dataset
	public Dataset copy(int amountOfEntries) {
		if(amountOfEntries == 0)
			amountOfEntries = 1;
		Set<Integer> lineNumbers = new TreeSet<>();
		int randomLineNumber;
		while(lineNumbers.size() < amountOfEntries) {
			randomLineNumber = (int) (Math.random() * entries());
			lineNumbers.add(randomLineNumber);
		}
		
		List<List<String>> subList = new ArrayList<>();
		for(Integer lineNumber : lineNumbers)
			subList.add(contents.get(lineNumber));
		
		return new Dataset(this.attributeNames, subList);
	}
	
	// creates a new Dataset consisting of entries which get removed from the initial Dataset
	public Dataset extract(int amountOfEntries) {
		if(amountOfEntries == 0)
			amountOfEntries = 1;
		int randomLineNumber;
		List<List<String>> subList = new ArrayList<>();
		for(int i = 0; i < amountOfEntries; i++) {
			randomLineNumber = (int) (Math.random() * entries());
			subList.add(contents.get(randomLineNumber));
			contents.remove(contents.get(randomLineNumber));
		}
		
		return new Dataset(this.attributeNames, subList);
	}
	
	public double calculateClassEntropy() {
		if(classEntropy == -1.0)
			classEntropy = calculateEntropy(this.classIndex);
		return classEntropy;
	}
	
	// calculates the entropy of the attribute at the given index
	public double calculateEntropy(int index) {
		// maps each possible value of the attribute to an occurrence counter
		Map<String, Integer> m = new HashMap<>();
		String currentElement;
		for(int entry = 0; entry < entries(); entry++) {
			currentElement = contents.get(entry).get(index);
			// if the currentElement (current value of the attribute) has already been found at least once, the counter gets increased by one
			if(m.containsKey(currentElement))
				m.put(currentElement, m.get(currentElement) + 1);
			// otherwise, the current value gets mapped with a counter starting at 1
			else
				m.put(currentElement, 1);
		}
		
		double entropy = 0;
		double currentProbability;
		for(String element : m.keySet()) {
			currentProbability = (double) m.get(element) / entries();
			entropy -= currentProbability * log2(currentProbability);
		}
		
		return entropy;
	}
	
	/**
	 * Calculates the Information Gain of the attribute at the given index
	 * The dataset gets split to parts - one for each value of the attribute
	 * Then, each part's class' entropy gets multiplied by the amount of the part's entries over the amount of initial entries
	 * (which is the probability of the part's attribute value appearing in the initial dataset)
	 * Each of the aforementioned products gets subtracted from the initial dataset's class' entropy
	 * The type is (where X is the attribute, x any of its values and C the class):
	 * IG(X, C) = H(C) - Σx[P(X=x)*H(C|X=x)]
	 * @param index The index of the attribute whose information gain will be calculated
 	 */
	public double calculateIG(int index) {
		// IG(X, C) = H(C) - Σc[P(C=c)*H(C|C=c)] = H(C), since H(C|C=c) = 0 (for any c)
		if(index == classIndex)
			return this.calculateClassEntropy();
		
		Map<String, Dataset> parts = this.split(index);
		double ig = this.calculateClassEntropy();
		double currentProbability;
		for(Dataset d : parts.values()) {
			currentProbability = (double) d.entries() / this.entries();
			ig -=  currentProbability * d.calculateClassEntropy();
		}
		
		return ig;
	}
	
	private static double log2(double x) {
		return (Math.log(x) / LOG_OF_TWO);		// Logb(x) = Loga(x)/Loga(b)
	}
	
	/**
	 * The dataset's class is excluded from the search
	 * @return The index of the highest information gain attribute
	 */
	public int indexOfHighestIGAtttribute() {
		int indexOfMax = -1;
		double maxIG = -1.0;
		double currentIG;
		for(int i = 0; i < attributes(); i++) {
			if(i == classIndex)
				continue;
			currentIG = calculateIG(i);
			if(currentIG > maxIG) {
				indexOfMax = i;
				maxIG = currentIG;
			}
		}
		return  indexOfMax;
	}
	
	public String nameOfHighestIGAtttribute() {
		int indexOfMax = -1;
		double maxIG = -1.0;
		double currentIG;
		for(int i = 0; i < attributes(); i++) {
			if(i == classIndex)
				continue;
			currentIG = calculateIG(i);
			if(currentIG > maxIG) {
				indexOfMax = i;
				maxIG = currentIG;
			}
		}
		return  attributeNames.get(indexOfMax);
	}
	
	public String mostCommonClassValue() {
		Map<String, Integer> counters = new TreeMap<>();
		String currentClassValue;
		for(List<String> entry : contents) {
			currentClassValue = entry.get(classIndex);
			if(counters.containsKey(currentClassValue))
				counters.put(currentClassValue, counters.get(currentClassValue) + 1);
			else
				counters.put(currentClassValue, 1);
		}
		
		int max = -1;
		int currentCounter;
		String mostCommonClassValue = null;
		for(String classValue : counters.keySet()) {
			currentCounter = counters.get(classValue);
			if(currentCounter > max) {
				max = currentCounter;
				mostCommonClassValue = classValue;
			}
		}
		
		return mostCommonClassValue;
	}
	
	public int getAttributeIndex(String attributeName) {
		return attributeNames.indexOf(attributeName);
	}
	
	public int attributes() {
		if(contents.isEmpty())
			return 0;
		return contents.get(0).size();
	}
	
	public int entries() {
		return contents.size();
	}
	
	public boolean isEmpty() {
		return this.contents.isEmpty();
	}
	
	public void printContents() {
		System.out.println("Contents: ");
		for(String name : attributeNames)
			System.out.print(fixWidth(name) + " ");
		System.out.println();
		for(List<String> entry : contents) {
			for(String attribute : entry)
				System.out.print(fixWidth(attribute) + " ");
			System.out.println();
		}
	}
	
	public void printInfo() {
		System.out.println("Class index: " + classIndex);
		
		printContents();
		
		System.out.println("Entropy: ");
		for(int i = 0; i < attributes(); i++)
			System.out.println(attributeNames.get(i) + ": " + calculateEntropy(i));
		
		System.out.println("IG: ");
		for(int i = 0; i < attributes(); i++)
			System.out.println(attributeNames.get(i) + ": " + calculateIG(i));
		System.out.println();
		
		
		System.out.println("Possible values: ");
		for(int i = 0; i < attributes(); i++)
			System.out.println(attributeNames.get(i) + ":\t " + possibleValues.get(i));
		
		System.out.println();
	}
	
	private String fixWidth(String s) {
		if(s.length() > 12)
			return (s.substring(0, 11) + ".");
		for(int i = s.length(); i < 12; i++)
			if(i % 2 == 0)
				s = s.concat(" ");
			else
				s = " ".concat(s);
		return s;
	}
	
}
