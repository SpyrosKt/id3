
import java.util.*;

public class DecisionTree {
	
	private class Node {
		
		private String decisiveAttribute;				// the name of the attribute based on which the dataset is split on this node
		Map<String, Node> children = new TreeMap<>();	// each child is mapped to the value of decisiveAttribute that appears in it
		String decision = null;							// non-null only in leaves
		
		Node() {}
		
		Node(String decisiveAttribute) {
			this.decisiveAttribute = decisiveAttribute;
		}
		
		Node(Dataset dataset) {
			this.decisiveAttribute = dataset.getAttributeName(dataset.indexOfHighestIGAtttribute());
		}
		
		public void setDecisiveAttribute(String decisiveAttribute) {
			this.decisiveAttribute = decisiveAttribute;
		}
		
		public void setDecision(String decision) {
			this.decision = decision;
		}
		
		// maybe return a copy?
		public Map<String, Node> getChildren() {
			return children;
		}
		
		public void addChild(Node child, String s) {
			children.put(s, child);
		}
		
		public boolean isDecisionNode() {
			return decision != null;
		}
		
		public String getDecisiveAttribute() {
			return decisiveAttribute;
		}
		
		public String getDecision() {
			return decision;
		}
		
		@Override
		public String toString() {
			if(decision != null)
				return decision;
			return (decisiveAttribute + "?");
		}
		
	}
	
	private Node root = new Node();
	private double threshold;
	
	/**
	 * Instead of expanding fully the tree, this constructor stops whenever the class' entropy (or the maximum IG?) reaches the threshold
	 * In such cases, the decision is taken according to the most common value in class
	 */
	DecisionTree(Dataset trainData, double threshold) {
		this.threshold = threshold;
		
		Queue<Node> queue = new LinkedList<>();
		Map<Node, Dataset> map = new HashMap<>();
		queue.add(root);
		map.put(root, trainData);
		
		Node currentNode;
		Dataset currentDataset;
		Map<String, Dataset> datasetParts;
		while(!queue.isEmpty()) {
			currentNode = queue.remove();
			currentDataset = map.get(currentNode);
			
			// in case the only attribute left is the class
			// or if the class' entropy is low enough (judging using threshold)
			// then instead of expanding the tree, the current node is set to be a decision node
			if(currentDataset.attributes() == 1 || currentDataset.calculateClassEntropy() <= threshold) {
				currentNode.setDecision(currentDataset.mostCommonClassValue());
			}
			else {
				currentNode.setDecisiveAttribute(currentDataset.nameOfHighestIGAtttribute());
				datasetParts = currentDataset.split();
				
				for(String key : datasetParts.keySet()) {
					Node child = new Node();
					currentNode.addChild(child, key);
					
					queue.add(child);
					map.put(child, datasetParts.get(key));
				}
			}
		}
	}
	
	DecisionTree(Dataset trainData) {
		this(trainData, 0);
	}
	
	public static DecisionTree defaultInstance(Dataset trainData, Dataset validationData, List<Double> thresholds) {
		if(thresholds.isEmpty() || validationData.isEmpty())
			return new DecisionTree(trainData);
		
		DecisionTree bestTree = new DecisionTree(trainData);
		Double maxScore = bestTree.test(validationData);
		
		DecisionTree currentTree;
		double currentScore;
		for(Double threshold : thresholds) {
			currentTree = new DecisionTree(trainData, threshold);
			currentScore = currentTree.test(validationData);
			
			if(currentScore > maxScore) {
				maxScore = currentScore;
				bestTree = currentTree;
			}
		}
		
		return bestTree;
	}
	
	public static DecisionTree defaultInstance(Dataset trainData, Dataset validationData, int amountOfTrees) {
		List<Double> randomizedThresholds = new ArrayList<>();
		for(int i = 0; i < amountOfTrees; i++)
			randomizedThresholds.add(Math.random());
		return defaultInstance(trainData, validationData, randomizedThresholds);
	}
	
	/**
	 * Tests the tree with the given dataset
	 * @return The precision ratio of the decision tree
	 */
	public double test(Dataset testData) {
		int correctDecisions = 0;
		
		for(int i = 0; i < testData.entries(); i++)
			if(testData.classOf(i).equals(makeDecision(testData.getMappedEntry(i))))
				correctDecisions++;
		
		return ((double) correctDecisions / testData.entries());
	}
	
	public double getThreshold() {
		return this.threshold;
	}
	
	private String makeDecision(Map<String, String> mappedEntry) {
		Node current = root;
		String decisiveAttributeValue;
		while(!current.isDecisionNode()) {
			decisiveAttributeValue = mappedEntry.get(current.getDecisiveAttribute());
			current = current.getChildren().get(decisiveAttributeValue);
			
			// if no child exists for decisiveAttributeValue, no decision can be made
			if(current == null)
				return null;
		}
		
		return current.decision;
	}
	
}
